package com.safelyyou.jiraexport.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

@SpringBootTest
@ActiveProfiles("local")
class JiraSearchServiceIntegrationTest {

    @Autowired
    private JiraSearchService jiraSearchService;

    @Test
    public void callsApi() throws IOException {
        jiraSearchService.searchJiraAndPaginate("project = JiraMetrics");
    }
}