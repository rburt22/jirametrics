package com.safelyyou.jiraexport.dto;

import lombok.Data;

import java.time.ZonedDateTime;

/**
 * The issue DTO for transferring the issue entity
 */
@Data
public class IssueDto {

    private String key;

    private String projectKey;

    private ZonedDateTime createDate;

    private ZonedDateTime startDate;

    private ZonedDateTime doneDate;

    private long mtr;

    private String type;

    private String priority;

}
