package com.safelyyou.jiraexport.dto;

import lombok.Builder;
import lombok.Data;

/**
 * An aggregation for defect escape rate
 */
@Data
@Builder
public class DefectAggregation {

    private String date;
    private long value;
    private int totalIssues;
    private int totalBugs;

}
