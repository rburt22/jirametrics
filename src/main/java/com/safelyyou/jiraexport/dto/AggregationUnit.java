package com.safelyyou.jiraexport.dto;

/**
 * The field unit to aggregate on
 */
public enum AggregationUnit {
    RESOLUTION,
    CREATION,
}
