package com.safelyyou.jiraexport.dto;

/**
 * The period to aggregate date on
 */
public enum AggregationPeriod {
    DAYS,
    WEEKS,
    MONTHS,
}
