package com.safelyyou.jiraexport.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Aggregated data for Total created and total resolved
 */
@Data
@Builder
public class TotalsAggregation {
    private String date;
    private int value;
}
