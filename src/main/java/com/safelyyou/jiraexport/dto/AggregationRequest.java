package com.safelyyou.jiraexport.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * A request to aggregate data
 */
@Data
public class AggregationRequest {
    private List<String> projects;
    private Date from;
    private Date to;
    private AggregationUnit unit;
    private AggregationPeriod period;
    private List<String> priorities;
}
