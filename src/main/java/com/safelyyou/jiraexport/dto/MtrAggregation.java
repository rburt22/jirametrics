package com.safelyyou.jiraexport.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * An aggregation of mean time to resolution
 */
@Data
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MtrAggregation {
    private String date;
    private long mtr;
}
