package com.safelyyou.jiraexport.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;

@Configuration
public class RetrofitConfig {

    @Bean
    public Retrofit retrofit(JiraConfigProperties jiraConfig,
                             ObjectMapper objectMapper) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(10, TimeUnit.SECONDS);
        builder.connectTimeout(5, TimeUnit.SECONDS);
        /*HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BODY);
        builder.addInterceptor(loggingInterceptor);*/
        // Here we add the email and token to each request so that we don't have
        // to add this header to all requests manually
        builder.addInterceptor(chain -> {
            String authToken = Credentials.basic(jiraConfig.getUserEmail(), jiraConfig.getApiToken());
            Request request = chain.request()
                    .newBuilder()
                    .addHeader("Authorization", authToken)
                    .build();
            return chain.proceed(request);
        });

        OkHttpClient client = builder.build();

        return new Retrofit.Builder()
                .baseUrl(jiraConfig.getBaseUrl())
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .build();
    }

}
