package com.safelyyou.jiraexport.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "jira")
@Data
public class JiraConfigProperties {
    private String baseUrl;
    private String userEmail;
    private String apiToken;
}
