package com.safelyyou.jiraexport.config;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

/**
 * A configuration for database when deployed in AWS
 */
@Configuration
@Profile("prod")
public class DatabaseConfig {

    @Bean
    public DataSource getDataSource(Environment env) {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url("jdbc:mysql://" + env.getProperty("RDS_HOSTNAME") + ":" + env.getProperty("RDS_PORT")
                + "/" + env.getProperty("RDS_DB_NAME"));
        dataSourceBuilder.username(env.getProperty("RDS_USERNAME"));
        dataSourceBuilder.password(env.getProperty("RDS_PASSWORD"));
        return dataSourceBuilder.build();
    }

}
