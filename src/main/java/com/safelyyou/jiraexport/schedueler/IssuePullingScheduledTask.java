package com.safelyyou.jiraexport.schedueler;

import com.safelyyou.jiraexport.service.MtrService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.stream.Collectors;

import static com.safelyyou.jiraexport.service.Constants.PROJECT_LIST_STRING;

/**
 * A task scheduled class that periodically polls Jira for issues to calculate MTR for
 */
@Component
@Slf4j
public class IssuePullingScheduledTask {

    private static final String projectKeyList = PROJECT_LIST_STRING.stream()
            .map(s -> "\"" + s + "\"")
            .collect(Collectors.joining(","));

    private final MtrService mtrService;

    public IssuePullingScheduledTask(MtrService mtrService) {
        this.mtrService = mtrService;
    }

    @Scheduled(cron = "${cron.expression}")
    public void executeDataFetch() throws IOException {
        String jql = generateJql();
        log.info("Running scheduled task for fetching MTR for jql: {}", jql);
        mtrService.calculateMtrForJql(jql);
        log.info("Done running scheduled task for fetching MTR for jql: {}", jql);
    }

    private String generateJql() {
        return "project in (" + projectKeyList + ")";
    }
}
