package com.safelyyou.jiraexport.retrofit;

import com.safelyyou.jiraexport.retrofit.request.SearchRequest;
import com.safelyyou.jiraexport.retrofit.response.SearchResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Retrofit service for calling Jira
 */
public interface RetrofitService {

    @POST("/rest/api/3/search")
    Call<SearchResponse> searchJiraWithJql(@Body SearchRequest request);

}
