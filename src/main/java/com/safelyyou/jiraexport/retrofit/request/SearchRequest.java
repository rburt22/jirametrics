package com.safelyyou.jiraexport.retrofit.request;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SearchRequest {
    private String jql;
    private List<String> expand;
    private int startAt;
    private int maxResults;
}
