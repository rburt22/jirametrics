package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

@Data
public class Assignee {
    private String accountId;
}
