package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

@Data
public class IssueType {
    private String name;
    private int id;
}
