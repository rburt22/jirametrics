package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

@Data
public class BugSource {
    private String value;
}
