package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

import java.util.List;

@Data
public class ChangeLog {
    private List<History> histories;
}
