package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

@Data
public class HistoryItem {
    private String field;
    private String fieldId;
    private String fromString;
    private String from;
    private String toString;
    private String to;
}
