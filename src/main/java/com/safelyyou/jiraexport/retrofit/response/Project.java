package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

@Data
public class Project {
    private String key;
    private String name;
}
