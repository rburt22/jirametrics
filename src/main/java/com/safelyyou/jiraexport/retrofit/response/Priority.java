package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

@Data
public class Priority {
    private String name;
    private int id;
}
