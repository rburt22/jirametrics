package com.safelyyou.jiraexport.retrofit.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SearchResponse {
    private String expand;
    private int startAt;
    private int maxResults;
    private int total;
    private List<Issue> issues;
}
