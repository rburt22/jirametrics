package com.safelyyou.jiraexport.retrofit.response;

import lombok.Data;

@Data
public class Issue {
    private int id;
    private String key;
    private Fields fields;
    private ChangeLog changelog;
}
