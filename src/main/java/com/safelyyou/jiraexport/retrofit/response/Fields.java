package com.safelyyou.jiraexport.retrofit.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class Fields {
    private IssueType issuetype;
    private Project project;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime created;
    private Priority priority;
    @JsonProperty("customfield_10526")
    private BugSource bugSource;
    private Assignee assignee;
}
