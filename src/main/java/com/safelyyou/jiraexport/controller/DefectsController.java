package com.safelyyou.jiraexport.controller;

import com.safelyyou.jiraexport.dto.AggregationRequest;
import com.safelyyou.jiraexport.dto.DefectAggregation;
import com.safelyyou.jiraexport.service.DefectAggregator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DefectsController {

    private final DefectAggregator defectAggregator;

    public DefectsController(DefectAggregator defectAggregator) {
        this.defectAggregator = defectAggregator;
    }


    @PostMapping("/aggregate-defects-external")
    public List<DefectAggregation> aggregateExternalDefects(@RequestBody AggregationRequest aggregationRequest) {
        return defectAggregator.calculateExternalDefectValues(aggregationRequest);
    }

    @PostMapping("/aggregate-defects-internal")
    public List<DefectAggregation> aggregateInternalDefects(@RequestBody AggregationRequest aggregationRequest) {
        return defectAggregator.calculateInternalDefectValues(aggregationRequest);
    }

}
