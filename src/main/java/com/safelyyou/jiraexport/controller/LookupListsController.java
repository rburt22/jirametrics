package com.safelyyou.jiraexport.controller;

import com.safelyyou.jiraexport.service.Constants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/lookup")
public class LookupListsController {

    @GetMapping("/projects-list")
    public List<String> getProjectsList() {
        return Constants.PROJECT_LIST_STRING;
    }

}
