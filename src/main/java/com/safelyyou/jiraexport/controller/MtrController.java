package com.safelyyou.jiraexport.controller;

import com.safelyyou.jiraexport.dto.AggregationRequest;
import com.safelyyou.jiraexport.dto.MtrAggregation;
import com.safelyyou.jiraexport.service.MtrAggregator;
import com.safelyyou.jiraexport.service.MtrService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MtrController {

    private final MtrAggregator mtrAggregator;
    private final MtrService mtrService;

    public MtrController(MtrAggregator mtrAggregator,
                         MtrService mtrService) {
        this.mtrAggregator = mtrAggregator;
        this.mtrService = mtrService;
    }


    @PostMapping(path = "/aggregate-data")
    public List<MtrAggregation> aggregateData(@RequestBody AggregationRequest aggregationRequest) {
        return mtrAggregator.aggregateIssuesForAll(aggregationRequest);
    }

    @PostMapping(path = "/aggregate-data-for-bugs")
    public List<MtrAggregation> aggregateDataForBugs(@RequestBody AggregationRequest aggregationRequest) {
        return mtrAggregator.aggregateIssuesForHighBugs(aggregationRequest);
    }

    @PostMapping("/execute-mtr-for-jql")
    public ResponseEntity<Void> executeMtrForJql(@RequestBody String jql) throws IOException {
        mtrService.calculateMtrForJql(jql);
        return ResponseEntity.noContent().build();
    }

}
