package com.safelyyou.jiraexport.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicAuthController {
    @PostMapping("/api/basicAuth")
    public String basicAuth() {
        return "success";
    }
}
