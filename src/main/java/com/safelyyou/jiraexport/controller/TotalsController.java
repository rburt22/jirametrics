package com.safelyyou.jiraexport.controller;

import com.safelyyou.jiraexport.dto.AggregationRequest;
import com.safelyyou.jiraexport.dto.TotalsAggregation;
import com.safelyyou.jiraexport.service.TotalNumberAggregator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TotalsController {

    private final TotalNumberAggregator totalNumberAggregator;

    public TotalsController(TotalNumberAggregator totalNumberAggregator) {
        this.totalNumberAggregator = totalNumberAggregator;
    }

    @PostMapping("/api/calculate-creation-totals")
    public List<TotalsAggregation> calculateCreationTotals(@RequestBody AggregationRequest aggregationRequest) {
        return totalNumberAggregator.calculateTotalsByCreationDate(aggregationRequest);
    }

    @PostMapping("/api/calculate-resolution-totals")
    public List<TotalsAggregation> calculateResolutionTotals(@RequestBody AggregationRequest aggregationRequest) {
        return totalNumberAggregator.calculateTotalsByResolutionDate(aggregationRequest);
    }

    @PostMapping("/api/calculate-open-totals")
    public List<TotalsAggregation> calculateOpenTotals(@RequestBody AggregationRequest aggregationRequest) {
        return totalNumberAggregator.calculateOpenTotals(aggregationRequest);
    }
}
