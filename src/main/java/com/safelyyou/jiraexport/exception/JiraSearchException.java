package com.safelyyou.jiraexport.exception;

public class JiraSearchException extends RuntimeException {
    public JiraSearchException(String message) {
        super(message);
    }
}
