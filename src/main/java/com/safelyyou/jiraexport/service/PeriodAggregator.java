package com.safelyyou.jiraexport.service;

import com.safelyyou.jiraexport.dto.AggregationUnit;
import com.safelyyou.jiraexport.entity.IssueEntity;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This service aggregates values by periods and returns maps of aggregations periods to issues in that period
 */
@Service
public class PeriodAggregator {

    /**
     * Aggregates the data for the list of issues by day
     *
     * @param issues The list of issues to aggregate for
     * @param unit   The unit of aggregation
     * @return The list of aggregated data
     */
    public Map<String, List<IssueEntity>> aggregateByDays(List<IssueEntity> issues, AggregationUnit unit) {
        return issues.stream()
                .collect(Collectors.groupingBy(getIssueAggregateKey(unit,
                        DateTimeFormatter.ISO_DATE),
                        () -> new TreeMap<>(String::compareTo),
                        Collectors.toList()));
    }

    /**
     * Aggregates data by weeks
     *
     * @param issues The list of issues to aggregate for
     * @param unit   The unit of aggregation
     * @return The Map of aggregated data
     */
    public Map<String, List<IssueEntity>> aggregateByWeeks(List<IssueEntity> issues, AggregationUnit unit) {
        return issues.stream()
                .collect(Collectors.groupingBy(getIssueAggregateKey(unit, DateTimeFormatter.ofPattern("yyyy-ww")),
                        () -> new TreeMap<>(String::compareTo),
                        Collectors.toList()));
    }

    /**
     * Aggregates data by months
     *
     * @param issues The list of issues to aggregate for
     * @param unit   The unit of aggregation
     * @return The map of aggregated data
     */
    public Map<String, List<IssueEntity>> aggregateByMonths(List<IssueEntity> issues, AggregationUnit unit) {
        return issues.stream()
                .collect(Collectors.groupingBy(getIssueAggregateKey(unit, DateTimeFormatter.ofPattern("yyyy/MM")),
                        () -> new TreeMap<>(String::compareTo),
                        Collectors.toList()));
    }


    /**
     * Generates the key to aggregate the issues on to be used in grouping the issues
     *
     * @param unit                  The unit of aggregation
     * @param aggregatePeriodFormat The period format to use for aggregate the key on
     * @return The aggregation key generation function for that issue
     */
    private Function<IssueEntity, String> getIssueAggregateKey(AggregationUnit unit,
                                                               DateTimeFormatter aggregatePeriodFormat) {
        return issueEntity -> {
            switch (unit) {
                case CREATION:
                    return issueEntity.getCreateDate().toLocalDate()
                            .format(aggregatePeriodFormat);
                case RESOLUTION:
                default:
                    return issueEntity.getDoneDate().toLocalDate()
                            .format(aggregatePeriodFormat);
            }
        };
    }

}
