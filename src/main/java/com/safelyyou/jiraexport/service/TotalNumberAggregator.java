package com.safelyyou.jiraexport.service;

import com.safelyyou.jiraexport.dto.AggregationPeriod;
import com.safelyyou.jiraexport.dto.AggregationRequest;
import com.safelyyou.jiraexport.dto.AggregationUnit;
import com.safelyyou.jiraexport.dto.TotalsAggregation;
import com.safelyyou.jiraexport.entity.IssueEntity;
import com.safelyyou.jiraexport.repository.IssueRepository;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A service to aggregate data by the total number of issues
 */
@Service
public class TotalNumberAggregator {

    private final IssueRepository issueRepository;
    private final PeriodAggregator periodAggregator;

    public TotalNumberAggregator(IssueRepository issueRepository,
                                 PeriodAggregator periodAggregator) {
        this.issueRepository = issueRepository;
        this.periodAggregator = periodAggregator;
    }

    /**
     * Aggregate by the total number of created issues
     *
     * @param aggregationRequest The aggregation request
     * @return aggregated data
     */
    public List<TotalsAggregation> calculateTotalsByCreationDate(AggregationRequest aggregationRequest) {
        var issues = issueRepository.findByCreateDateAndProjectKeyAndPriorities(
                aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getProjects(),
                aggregationRequest.getPriorities()
        );

        return aggregateData(issues, aggregationRequest.getPeriod(), AggregationUnit.CREATION);
    }

    /**
     * Aggregate by the total number of resolved issues
     *
     * @param aggregationRequest The aggregation request
     * @return aggregated data
     */
    public List<TotalsAggregation> calculateTotalsByResolutionDate(AggregationRequest aggregationRequest) {
        var issues = issueRepository.findByDoneDateAndProjectKeyAndPriorities(
                aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getProjects(),
                aggregationRequest.getPriorities()
        );
        return aggregateData(issues, aggregationRequest.getPeriod(), AggregationUnit.RESOLUTION);
    }

    public List<TotalsAggregation> calculateOpenTotals(AggregationRequest aggregationRequest) {
        var issues = issueRepository.findOpenIssuesCreatedBefore(
                aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getProjects(),
                aggregationRequest.getPriorities()
        );
        return aggregateData(issues, aggregationRequest.getPeriod(), AggregationUnit.CREATION);
    }

    private List<TotalsAggregation> aggregateData(List<IssueEntity> issues,
                                                  AggregationPeriod period,
                                                  AggregationUnit unit) {
        Map<String, List<IssueEntity>> aggregatedIssues;
        switch (period) {
            case WEEKS:
                aggregatedIssues = periodAggregator.aggregateByWeeks(issues, unit);
                break;
            case MONTHS:
                aggregatedIssues = periodAggregator.aggregateByMonths(issues, unit);
                break;
            default:
            case DAYS:
                aggregatedIssues = periodAggregator.aggregateByDays(issues, unit);
                break;
        }
        return aggregatedIssues.entrySet()
                .stream()
                .map(entry -> {
                    var issuesInPeriod = entry.getValue().size();
                    return TotalsAggregation.builder()
                            .date(entry.getKey())
                            .value(issuesInPeriod)
                            .build();
                })
                .collect(Collectors.toList());
    }
}
