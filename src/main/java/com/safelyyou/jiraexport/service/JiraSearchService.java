package com.safelyyou.jiraexport.service;

import com.safelyyou.jiraexport.exception.JiraSearchException;
import com.safelyyou.jiraexport.retrofit.RetrofitService;
import com.safelyyou.jiraexport.retrofit.request.SearchRequest;
import com.safelyyou.jiraexport.retrofit.response.Issue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This service class can be used to search Jira for issues against a JQL
 */
@Service
@Slf4j
public class JiraSearchService {

    private final RetrofitService retrofitService;

    public JiraSearchService(Retrofit retrofit) {
        retrofitService = retrofit.create(RetrofitService.class);
    }

    /**
     * Searches Jira issues against a certain JQL string
     *
     * @param jql The JQL string to use for filtration
     * @return The list of issues matching that JQL (can be empty)
     * @throws IOException If an issue happened while calling Jira
     */
    public List<Issue> searchJiraAndPaginate(String jql) throws IOException {
        int currentStart = 0,
                total = Integer.MAX_VALUE,
                maxResults = 1000;
        List<Issue> issues = new ArrayList<>();
        while (currentStart < total) {
            var issuesCall = retrofitService.searchJiraWithJql(
                    SearchRequest.builder()
                            .jql(jql)
                            .startAt(currentStart)
                            .maxResults(maxResults)
                            .expand(List.of("changelog", "renderedFields"))
                            .build()
            );
            var response = issuesCall.execute();
            if (!response.isSuccessful()) {
                throw new JiraSearchException(response.errorBody() != null
                        ? response.errorBody().string() : "Unknown error");
            }
            var result = response.body();
            log.debug("Found {} for JQL, currently we start at {} to {}", result.getTotal(),
                    result.getStartAt(),
                    result.getTotal() - result.getStartAt() - 1);

            issues.addAll(result.getIssues());
            maxResults = result.getMaxResults();
            currentStart = currentStart + result.getMaxResults();
            total = result.getTotal();
        }

        return issues;
    }
}
