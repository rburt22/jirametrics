package com.safelyyou.jiraexport.service;

import com.safelyyou.jiraexport.dto.AggregationRequest;
import com.safelyyou.jiraexport.dto.MtrAggregation;
import com.safelyyou.jiraexport.entity.IssueEntity;
import com.safelyyou.jiraexport.repository.IssueRepository;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.safelyyou.jiraexport.service.Constants.BUG_TYPE;
import static com.safelyyou.jiraexport.service.Constants.PRIORITIES_FOR_HIGH_AND_HIGHEST;

/**
 * A service to aggregate issues mean time to resolution by Days, weeks, or months
 */
@Service
public class MtrAggregator {

    private final IssueRepository issueRepository;
    private final PeriodAggregator periodAggregator;

    public MtrAggregator(IssueRepository issueRepository,
                         PeriodAggregator periodAggregator) {
        this.issueRepository = issueRepository;
        this.periodAggregator = periodAggregator;
    }

    /**
     * Aggregates the MTR for high and highest bugs
     *
     * @param aggregationRequest The aggregation request
     * @return A list of aggregated data
     */
    public List<MtrAggregation> aggregateIssuesForHighBugs(AggregationRequest aggregationRequest) {
        var issues = issueRepository.findDoneDateProjectKeyTypeAndPriority(
                aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getProjects(),
                BUG_TYPE,
                PRIORITIES_FOR_HIGH_AND_HIGHEST
        );

        return calculateMtrAggregationForIssues(issues, aggregationRequest);
    }

    /**
     * Aggregates MTR data from the database for issues matching the aggregation request
     *
     * @param aggregationRequest The aggregation request to aggregate for
     * @return The list of aggregation date matching the request, can be empty
     */
    public List<MtrAggregation> aggregateIssuesForAll(AggregationRequest aggregationRequest) {
        List<IssueEntity> issues;
        switch (aggregationRequest.getUnit()) {
            case CREATION:
                issues = issueRepository.findByCreateDateAndProjectKeyAndPriorities(
                        aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                        aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                        aggregationRequest.getProjects(),
                        aggregationRequest.getPriorities()
                );
                break;
            case RESOLUTION:
            default:
                issues = issueRepository.findByDoneDateAndProjectKeyAndPriorities(
                        aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                        aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                        aggregationRequest.getProjects(),
                        aggregationRequest.getPriorities()
                );
        }

        return calculateMtrAggregationForIssues(issues, aggregationRequest);
    }

    private List<MtrAggregation> calculateMtrAggregationForIssues(List<IssueEntity> issues,
                                                                  AggregationRequest aggregationRequest) {
        Map<String, List<IssueEntity>> aggregatedIssues;
        switch (aggregationRequest.getPeriod()) {
            case WEEKS:
                aggregatedIssues = periodAggregator.aggregateByWeeks(issues, aggregationRequest.getUnit());
                break;
            case MONTHS:
                aggregatedIssues = periodAggregator.aggregateByMonths(issues, aggregationRequest.getUnit());
                break;
            default:
            case DAYS:
                aggregatedIssues = periodAggregator.aggregateByDays(issues, aggregationRequest.getUnit());
                break;
        }
        return aggregatedIssues
                .entrySet()
                .stream()
                .map(this::calculateMtr)
                .collect(Collectors.toList());
    }

    /**
     * Calculates the MTR for an aggregate of issues
     *
     * @param entry The entry list containing the aggregation key and the list of issues in that key
     * @return The aggregation data for that entry
     */
    private MtrAggregation calculateMtr(Map.Entry<String, List<IssueEntity>> entry) {
        long totalMtr = entry.getValue().stream().mapToLong(IssueEntity::getMtr).sum();
        long avg = totalMtr / entry.getValue().size();
        return new MtrAggregation(entry.getKey(), avg);
    }


}
