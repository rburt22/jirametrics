package com.safelyyou.jiraexport.service;

import com.safelyyou.jiraexport.dto.IssueDto;
import com.safelyyou.jiraexport.entity.IssueEntity;
import com.safelyyou.jiraexport.repository.IssueRepository;
import com.safelyyou.jiraexport.retrofit.response.BugSource;
import com.safelyyou.jiraexport.retrofit.response.History;
import com.safelyyou.jiraexport.retrofit.response.Issue;
import com.safelyyou.jiraexport.transformers.IssueDtoMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.safelyyou.jiraexport.service.Constants.BUG_TYPE;
import static com.safelyyou.jiraexport.service.Constants.INTERNAL_BUG_SOURCE;

/**
 * This service can be used to calculate the Mean time to resolution for issues and store in the database
 */
@Service
@Slf4j
public class MtrService {

    public static final String STATUS_FIELD = "status";
    private static final String ASSIGNEE_FIELD = "assignee";
    /**
     * By default if the issues isn't worked on or moved back to in progress, then we say it is -1
     */
    private static final int DEFAULT_MTR_NOT_DONE = -1;
    private static final String IN_PROGRESS = "in progress";
    private static final String DONE_STATUS = "Done";

    private final JiraSearchService jiraSearchService;
    private final IssueRepository issueRepository;
    private final IssueDtoMapper issueDtoMapper;

    public MtrService(JiraSearchService jiraSearchService,
                      IssueRepository issueRepository) {
        this.jiraSearchService = jiraSearchService;
        this.issueRepository = issueRepository;
        issueDtoMapper = IssueDtoMapper.INSTANCE;
    }

    /**
     * Find and return the MTR for all issues matching this JQL
     *
     * @param jql The JQL string to match against
     * @return The matched issues
     * @throws IOException If retrofit failed to work
     */
    public List<IssueDto> calculateMtrForJql(String jql) throws IOException {
        log.info("Calculating MTR for issues matching JQL: {}", jql);
        var issues = jiraSearchService.searchJiraAndPaginate(jql);
        log.info("Found {} issues matching JQL", issues.size());
        return issues.stream()
                .map(this::findMtrForIssue)
                .collect(Collectors.toList());
    }

    /**
     * Calculate the MTR for an issue
     *
     * @param issue The Jira issue to calculate the mean time fore
     * @return The issue entity with data calculated
     */
    public IssueDto findMtrForIssue(Issue issue) {
        var issueEntity = new IssueEntity();
        setIssueBasicFields(issue, issueEntity);

        var historiesResult = findFirstInProgressDate(issue.getChangelog().getHistories());

        issueEntity.setStartDate(historiesResult.getFirstInProgressDate().orElse(null));

        issueEntity.setDoneDate(historiesResult.getLastDoneDate().orElse(null));

        if (historiesResult.getLastDoneDate().isPresent()) {
            Duration duration;
            if (historiesResult.getFirstInProgressDate().isPresent()
                    && historiesResult.getLastDoneDate().get().isAfter(
                    historiesResult.getFirstInProgressDate().get())
            ) {
                // Both dates are present and it was moved to done after the first time it moved to in-progress
                duration = Duration.between(historiesResult.getFirstInProgressDate().get(),
                        historiesResult.getLastDoneDate().get());
            } else {
                //If there is a done date but there is start date, then we take the create date for the calculations
                duration = Duration.between(issueEntity.getCreateDate(), historiesResult.getLastDoneDate().get());
            }
            issueEntity.setMtr(duration.toMinutes());
        } else {
            issueEntity.setMtr(DEFAULT_MTR_NOT_DONE);
        }
        Set<String> assignees = new HashSet<>(historiesResult.getAssignees());
        if (issue.getFields().getAssignee() != null
                && issue.getFields().getAssignee().getAccountId() != null) {
            assignees.add(issue.getFields().getAssignee().getAccountId());
        }
        issueEntity.setAssignees(String.join(",", assignees));
        issueRepository.save(issueEntity);
        return issueDtoMapper.toIssueDto(issueEntity);
    }

    private void setIssueBasicFields(Issue issue, IssueEntity issueEntity) {
        issueEntity.setProjectKey(issue.getFields().getProject().getKey());
        issueEntity.setProjectName(issue.getFields().getProject().getName());
        issueEntity.setKey(issue.getKey());
        issueEntity.setCreateDate(issue.getFields().getCreated());
        issueEntity.setType(issue.getFields().getIssuetype().getName());
        issueEntity.setTypeId(issue.getFields().getIssuetype().getId());
        if (issue.getFields().getPriority() != null) {
            issueEntity.setPriority(issue.getFields().getPriority().getName());
            issueEntity.setPriorityId(issue.getFields().getPriority().getId());
        }
        BugSource bugSource = issue.getFields().getBugSource();
        if (bugSource == null && issueEntity.getType().equalsIgnoreCase(BUG_TYPE)) {
            issueEntity.setBugSource(INTERNAL_BUG_SOURCE);
        } else if (bugSource != null) {
            issueEntity.setBugSource(bugSource.getValue());
        }
    }

    /**
     * Looks for the first date it was in-progress
     *
     * @param histories The issue's <code>ChangeHistory</code> list
     * @return The first time it was moved to in-progress, can be empty
     */
    private HistoriesResult findFirstInProgressDate(List<History> histories) {
        ZonedDateTime firstInProgressDate = null;
        ZonedDateTime lastDoneDate = null;
        Set<String> assignees = new HashSet<>();
        for (var history : histories) {
            for (var item : history.getItems()) {
                if (item.getField().equalsIgnoreCase(STATUS_FIELD)
                        && item.getFieldId().equalsIgnoreCase(STATUS_FIELD)) {
                    if (item.getToString().equalsIgnoreCase(IN_PROGRESS)) {
                        if (firstInProgressDate == null) {
                            firstInProgressDate = history.getCreated();
                        }
                        if (history.getCreated().isBefore(firstInProgressDate)) {
                            firstInProgressDate = history.getCreated();
                        }
                    } else if (item.getToString().equalsIgnoreCase(DONE_STATUS)) {
                        if (lastDoneDate == null) {
                            lastDoneDate = history.getCreated();
                        }
                        if (history.getCreated().isAfter(lastDoneDate)) {
                            lastDoneDate = history.getCreated();
                        }
                    }
                } else if (item.getField().equalsIgnoreCase(ASSIGNEE_FIELD)
                        && item.getFieldId().equalsIgnoreCase(ASSIGNEE_FIELD)) {
                    if (item.getFrom() != null) {
                        assignees.add(item.getFrom());
                    }
                    if (item.getTo() != null) {
                        assignees.add(item.getTo());
                    }
                }
            }
        }
        return new HistoriesResult(Optional.ofNullable(firstInProgressDate),
                Optional.ofNullable(lastDoneDate),
                assignees);
    }


    @Getter
    @Setter
    @AllArgsConstructor
    private static class HistoriesResult {
        private Optional<ZonedDateTime> firstInProgressDate;
        private Optional<ZonedDateTime> lastDoneDate;
        private Set<String> assignees;
    }
}
