package com.safelyyou.jiraexport.service;

import com.safelyyou.jiraexport.dto.AggregationRequest;
import com.safelyyou.jiraexport.dto.AggregationUnit;
import com.safelyyou.jiraexport.dto.DefectAggregation;
import com.safelyyou.jiraexport.entity.IssueEntity;
import com.safelyyou.jiraexport.repository.IssueRepository;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.safelyyou.jiraexport.service.Constants.*;

/**
 * Data aggregator for defects escape rate
 */
@Service
public class DefectAggregator {
    private final IssueRepository issueRepository;
    private final PeriodAggregator periodAggregator;

    public DefectAggregator(IssueRepository issueRepository,
                            PeriodAggregator periodAggregator) {
        this.issueRepository = issueRepository;
        this.periodAggregator = periodAggregator;
    }

    public List<DefectAggregation> calculateInternalDefectValues(AggregationRequest aggregationRequest) {
        return calculateDefectValues(aggregationRequest, INTERNAL_BUG_SOURCE);
    }

    public List<DefectAggregation> calculateExternalDefectValues(AggregationRequest aggregationRequest) {
        return calculateDefectValues(aggregationRequest, EXTERNAL_BUG_SOURCE);
    }

    private List<DefectAggregation> calculateDefectValues(AggregationRequest aggregationRequest, String bugSource) {
        var issues = issueRepository.findByDoneDateAndProjectKey(
                aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getProjects()
        );

        var bugs = issueRepository.findByCreatedDateAndTypeAndBugSource(
                aggregationRequest.getFrom().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getTo().toInstant().atZone(ZoneId.systemDefault()),
                aggregationRequest.getProjects(),
                BUG_TYPE,
                bugSource,
                PRIORITIES_FOR_HIGH_AND_HIGHEST
        );

        Map<String, List<IssueEntity>> aggregatedIssues, aggregatedBugs;
        switch (aggregationRequest.getPeriod()) {
            case WEEKS:
                aggregatedBugs = periodAggregator.aggregateByWeeks(bugs, AggregationUnit.CREATION);
                aggregatedIssues = periodAggregator.aggregateByWeeks(issues, AggregationUnit.RESOLUTION);
                break;
            case MONTHS:
                aggregatedBugs = periodAggregator.aggregateByMonths(bugs, AggregationUnit.CREATION);
                aggregatedIssues = periodAggregator.aggregateByMonths(issues, AggregationUnit.RESOLUTION);
                break;
            default:
            case DAYS:
                aggregatedBugs = periodAggregator.aggregateByDays(bugs, AggregationUnit.CREATION);
                aggregatedIssues = periodAggregator.aggregateByDays(issues, AggregationUnit.RESOLUTION);
                break;
        }
        return calculateDefectAggregation(aggregatedIssues, aggregatedBugs);
    }

    private List<DefectAggregation> calculateDefectAggregation(Map<String, List<IssueEntity>> issues,
                                                               Map<String, List<IssueEntity>> bugs) {
        return issues.entrySet()
                .stream()
                .map(entry -> {
                    var bugsInPeriod = bugs.getOrDefault(entry.getKey(), Collections.emptyList()).size();
                    var issuesInPeriod = entry.getValue().size();
                    return DefectAggregation.builder()
                            .date(entry.getKey())
                            //Rate is multiplied by 100 to remove 0 factors
                            .value((long) Math.ceil(((float) bugsInPeriod / issuesInPeriod) * 100))
                            .totalIssues(issuesInPeriod)
                            .totalBugs(bugsInPeriod)
                            .build();
                })
                .collect(Collectors.toList());
    }

}
