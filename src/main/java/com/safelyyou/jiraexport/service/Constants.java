package com.safelyyou.jiraexport.service;

import java.util.Arrays;
import java.util.List;

/**
 * Service constants
 */
public class Constants {
    public static final String BUG_TYPE = "Bug";
    public static final String INTERNAL_BUG_SOURCE = "Internal";
    public static final String EXTERNAL_BUG_SOURCE = "External";
    public static final List<String> PRIORITIES_FOR_HIGH_AND_HIGHEST = Arrays.asList("High", "Highest");

    /**
     * Used for pulling data
     */
    public static final List<String> PROJECT_LIST_STRING = List.of(
            "SU20",
            "AI Unified",
            "discover-ui",
            "Sky",
            "Guardian",
            "alarm-tagging-api",
            "alarm-tagging-ui");
}
