package com.safelyyou.jiraexport.transformers;

import com.safelyyou.jiraexport.dto.IssueDto;
import com.safelyyou.jiraexport.entity.IssueEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * A mapper class to map IssueDto to IssueEntity, uses {@link Mapper} from mapstruct
 */
@Mapper
public interface IssueDtoMapper {

    IssueDtoMapper INSTANCE = Mappers.getMapper(IssueDtoMapper.class);

    IssueDto toIssueDto(IssueEntity issueEntity);

}
