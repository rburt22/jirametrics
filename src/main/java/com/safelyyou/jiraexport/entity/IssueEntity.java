package com.safelyyou.jiraexport.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;

@Data
@Entity
@Table(name = "issue")
public class IssueEntity {

    @Id
    private String key;

    private String projectKey;

    private String projectName;

    private ZonedDateTime createDate;

    private ZonedDateTime startDate;

    private ZonedDateTime doneDate;

    private long mtr;

    private String type;

    private int typeId;

    private String priority;

    private int priorityId;

    private String bugSource;

    @Column(length = 5000)
    private String assignees;
}
