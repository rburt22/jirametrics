package com.safelyyou.jiraexport;

import com.safelyyou.jiraexport.config.JiraConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(JiraConfigProperties.class)
public class JiraExportApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraExportApplication.class, args);
    }
}
