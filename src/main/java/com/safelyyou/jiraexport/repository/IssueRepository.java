package com.safelyyou.jiraexport.repository;

import com.safelyyou.jiraexport.entity.IssueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Issue Spring JPA repository class
 */
@Repository
public interface IssueRepository extends JpaRepository<IssueEntity, String> {

    @Query("select i from IssueEntity i where i.doneDate between :from and :to " +
            " and (i.projectKey in :projects or i.projectName in :projects)" +
            " order by i.doneDate asc")
    List<IssueEntity> findByDoneDateAndProjectKey(ZonedDateTime from,
                                                  ZonedDateTime to,
                                                  List<String> projects);

    @Query("select i from IssueEntity i where i.doneDate between :from and :to " +
            " and (i.projectKey in :projects or i.projectName in :projects)" +
            " and i.priority in :priorities" +
            " order by i.doneDate asc")
    List<IssueEntity> findByDoneDateAndProjectKeyAndPriorities(ZonedDateTime from,
                                                               ZonedDateTime to,
                                                               List<String> projects,
                                                               List<String> priorities);

    @Query("select i from IssueEntity i where i.createDate between :from and :to " +
            " and (i.projectKey in :projects or i.projectName in :projects)" +
            " order by i.createDate asc")
    List<IssueEntity> findByCreateDateAndProjectKey(ZonedDateTime from,
                                                    ZonedDateTime to,
                                                    List<String> projects);

    @Query("select i from IssueEntity i where i.createDate between :from and :to " +
            " and (i.projectKey in :projects or i.projectName in :projects)" +
            " and i.priority in :priorities" +
            " order by i.createDate asc")
    List<IssueEntity> findByCreateDateAndProjectKeyAndPriorities(ZonedDateTime from,
                                                                 ZonedDateTime to,
                                                                 List<String> projects,
                                                                 List<String> priorities);

    @Query("select i from IssueEntity i where i.doneDate between :from and :to " +
            " and (i.projectKey in :projects or i.projectName in :projects)" +
            " and i.type = :type" +
            " and i.priority in :priorities" +
            " order by i.doneDate asc")
    List<IssueEntity> findDoneDateProjectKeyTypeAndPriority(ZonedDateTime from,
                                                            ZonedDateTime to,
                                                            List<String> projects,
                                                            String type,
                                                            List<String> priorities);

    @Query("select i from IssueEntity  i where " +
            " i.createDate between :from and :to" +
            " and (i.projectKey in :projects or i.projectName in :projects)" +
            " and i.type = :type" +
            " and i.bugSource = :bugSource" +
            " and i.priority in :priorities" +
            " order by i.createDate asc")
    List<IssueEntity> findByCreatedDateAndTypeAndBugSource(ZonedDateTime from,
                                                           ZonedDateTime to,
                                                           List<String> projects,
                                                           String type,
                                                           String bugSource,
                                                           List<String> priorities);

    @Query("select i from IssueEntity i where " +
            " i.createDate between :from and :to" +
            " or (i.createDate < :from and (i.doneDate >= :from" +
            "     or i.doneDate is null))" +
            " and (i.projectKey in :projects or i.projectName in :projects)" +
            " and i.priority in :priorities" +
            " order by i.createDate asc"
    )
    List<IssueEntity> findOpenIssuesCreatedBefore(ZonedDateTime from,
                                                  ZonedDateTime to,
                                                  List<String> projects,
                                                  List<String> priorities);

}
