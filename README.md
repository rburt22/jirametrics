# Jira Export

## Running locally


### Running the backend
1. Install JDK 11 and Maven 3
2. Add `JIRA_USER_EMAIL` this is your atlassian email, and `JIRA_API_TOKEN` this is your API token
 to your environment variables with respective values:
 For example: 
     ```
   export JIRA_USER_EMAIL=email@example.com && export JIRA_API_TOKEN={your_token}
    ```
3. Run `mvn spring-boot:run` at the root directory of the project and wait for spring to startup, the project will be available at 
  [localhost:8080](http://localhost:8080/)

### Running the frontend
1. Install `nodejs` 12+ and `yarn`
2. Switch working directory to `frontend` from the project root
3. Run `yarn` to install all the packages
4. Run `yarn start` to start the local frontend server
5. It will be available at [localhost:3000](http://localhost:3000)

---
## Running from the maven built jar 
You can also generate a deployable `jar` that is optimised for production and runs a single server

1. Run `maven clean install -DskipTests` in the project root
2. A file called `jira-export-0.0.1-SNAPSHOT.jar` will be generated in the `target` folder (or a similar jar file name with a different version)
3. Run `java -jar jira-export-0.0.1-SNAPSHOT.jar` at that directory, and 
   it will spin up a single server with both the front end and the backend available at [localhost:8080](http://localhost:8080)
4. You then deploy this `jar` file to any server

---

# Backend details

## Endpoints
All endpoints are preceded with `/api`

* `/execute-mtr-for-jql`: Calculates the MTR for a certain JQL string and fetches stores data
* `/aggregate-data`: Calculates aggregations for data according to a certain request

## Main entry points:

The project's directory is as follows under the `src/main/java` directory
```
|-- config -> Where all Spring config classes are
|-- controller -> All the MVC controllers are here
|-- dto -> Data transfer objects, which are used to transfer data to API clients
|-- entity -> The JPA database entities
|-- exception -> The custom exceptions used by the app
|-- repository -> All spring JPA repositories or data accessing repositories
|-- retrofit -> Retrofit related classes, this includes clients and all request and response objects
|-- scheduler -> The schedulers used to run data scrapping at certain intervals
|-- service -> The business object service layer
|-- transformers -> The DTO to entity transformers 
```

### Main classes

1. `JiraExportApplication` Is the main spring entry point and doesn't contain any business logic, 
  just basic configuration
2. `IssuePullingScheduledTask` This class periodically polls Jira for new issues,
  it uses a string to get the project keys to poll for
3. `MtrService` This service is used by the `IssuePullingScheduledTask` to store and calculate the MTR for issues. 
  It is also being used by the `SearchService` to store and fetch issues for a certain JQL.
4. `SearchService` used by the `execute-mtr-for-jql` to fetch and store MTR data for a JQL.

### H2 database

We use H2 database for local dev and we have the following settings:

1. We use file `jira_extract_data.data` at the user's home directory to store the data
2. You can access the console [here](http://localhost:8080/h2-console)
3. Password is `password` the default one

---

# Frontend details

## Main entry points
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
It has some directory structure a little different from React's one here:
```
|-- api -> Contains the API caller modules
|-- components -> The react components used in other views
|-- state -> For managing the Redux store's state
|-- views -> Are the views of the app, which resemble pages 
```

The main classes are from the `create-react-app` structure

### Main components
1. `HomeView`: The component which contains everything used to display the home page and the layout.
2. `AggregationForm`: A form component which contains data to be used in aggregation.
3. `AllMtrChart`: A component which renders the chart, and it uses `nivo` React charts.

### Redux's data:
1. `allMtrSlice`: The store reducers for MTR data
2. `store` the main store file
---

# Process flows:
1. _As a future consumer of dashboards I would like an extract of the issues data that include the MTRs_
   1. We load up the chart at the base component `HomeView` in the base URL
   2. We then make a request using the `AggregationForm`
   3. It sends a `fetchAggregates` action to redux to call the backend's API `/aggregate-data`
   4. The backend aggregates the data using the `IssueMtrAggregator` and returns the list of aggregation data to the frontend
   5. Redux then sends `fetchAggregates.fulfilled` action to store the data for the `allMtrSlice`
   6. The chart then updates and plots the data accordingly
   

---
## Deploying to AWS

1. From root directory run `mvn clean install -DskipTests`
2. A `jar` file `jira-export-0.0.1-SNAPSHOT.jar` will be generated in `target` folder.
3. Log into AWS and go to the `JiraExport-env` environment.
4. Click _Upload and deploy_ and select that `jar` file to upload.
5. Wait for the upload and the environment to restart




