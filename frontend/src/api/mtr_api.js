import axios from 'axios';

export const aggregateMtr = (aggregationRequest) => {
    return axios.post('/api/aggregate-data', aggregationRequest);
}

export const aggregateBugsMtr = (aggregationRequest) => {
    return axios.post('/api/aggregate-data-for-bugs', aggregationRequest);
}