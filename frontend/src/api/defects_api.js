import axios from "axios";

export const aggregateExternalDefects = (aggregationRequest) => {
    return axios.post('/api/aggregate-defects-external', aggregationRequest);
}

export const aggregateInternalDefects = (aggregationRequest) => {
    return axios.post('/api/aggregate-defects-internal', aggregationRequest);
}