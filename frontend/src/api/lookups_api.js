import axios from "axios";

export const getProjectsList = () => {
    return axios.get('/api/lookup/projects-list');
}