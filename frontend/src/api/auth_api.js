import axios from 'axios';

export const login = (credentials) => {
    return axios.post('/api/basicAuth', {},
        {
            auth: credentials
        });
}