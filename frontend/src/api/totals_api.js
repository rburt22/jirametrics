import axios from 'axios';

export const aggregateCreationTotals = (aggregationRequest) => {
    return axios.post('/api/calculate-creation-totals', aggregationRequest);
}

export const aggregateResolutionTotals = (aggregationRequest) => {
    return axios.post('/api/calculate-resolution-totals', aggregationRequest);
}

export const aggregateOpenTotals = (aggregationRequest) => {
    return axios.post('/api/calculate-open-totals', aggregationRequest);
}