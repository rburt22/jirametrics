import React from 'react';
import HomeView from "./views/HomeView";
import {Redirect, Route, Switch} from "react-router-dom";
import LoginView from "./views/Login";
import {useSelector} from "react-redux";
import {isLoggedInSelector} from "./state/reducers/authSlice";

function App() {
    const isLoggedIn = useSelector(isLoggedInSelector);

    function PrivateRoute({children, ...rest}) {
        return (
            <Route
                {...rest}
                render={({location}) =>
                    isLoggedIn ? (
                        children
                    ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: {from: location}
                            }}
                        />
                    )
                }
            />
        );
    }

    return (
        <Switch>
            <PrivateRoute exact path="/">
                <HomeView/>
            </PrivateRoute>
            <Route
                exact
                path="/login"
                render={() => (
                    <LoginView/>
                )}
            />
        </Switch>
    );
}

export default App;
