import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {aggregateExternalDefects} from '../../api/defects_api';


export const fetchExternalDefects = createAsyncThunk('aggregates/fetchExternalDefects',
    async (data) => {
        const response = await aggregateExternalDefects(data);
        return {
            data: response.data,
            period: data.period
        }
    })

export const externalDefectsSlice = createSlice({
    name: "externalDefects",
    initialState: {
        data: [],
        period: 'DAYS',
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchExternalDefects.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.data = action.payload.data
            state.period = action.payload.period
        },
        [fetchExternalDefects.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default externalDefectsSlice.reducer;

export const externalDefectsAggregationsSelector = state => {
    return state.externalDefects.data;
}

export const externalDefectsPeriodSelector = state => state.externalDefects.period;