import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {aggregateResolutionTotals} from '../../api/totals_api';


export const fetchResolutionTotals = createAsyncThunk('aggregates/fetchResolutionTotals',
    async (data) => {
        const response = await aggregateResolutionTotals(data);
        return {
            data: response.data,
            period: data.period
        }
    })

export const resolutionTotalsSlice = createSlice({
    name: "resolutionTotals",
    initialState: {
        data: [],
        period: 'DAYS',
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchResolutionTotals.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.data = action.payload.data
            state.period = action.payload.period
        },
        [fetchResolutionTotals.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default resolutionTotalsSlice.reducer;

export const resolutionTotalsSelector = state => {
    return state.resolutionTotals.data;
}

export const resolutionTotalsPeriodSelector = state => state.resolutionTotals.period;