import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {aggregateOpenTotals} from '../../api/totals_api';


export const fetchOpenTotals = createAsyncThunk('aggregates/fetchOpenTotals',
    async (data) => {
        const response = await aggregateOpenTotals(data);
        return {
            data: response.data,
            period: data.period
        }
    })

export const openTotalsSlice = createSlice({
    name: "openTotals",
    initialState: {
        data: [],
        period: 'DAYS',
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchOpenTotals.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.data = action.payload.data
            state.period = action.payload.period
        },
        [fetchOpenTotals.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default openTotalsSlice.reducer;

export const openTotalsSelector = state => {
    return state.openTotals.data;
}

export const openTotalsPeriodSelector = state => state.openTotals.period;