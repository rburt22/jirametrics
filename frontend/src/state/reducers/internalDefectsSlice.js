import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {aggregateInternalDefects} from '../../api/defects_api';


export const fetchInternalDefects = createAsyncThunk('aggregates/fetchInternalDefects',
    async (data) => {
        const response = await aggregateInternalDefects(data);
        return {
            data: response.data,
            period: data.period
        }
    })

export const internalDefectsSlice = createSlice({
    name: "internalDefects",
    initialState: {
        data: [],
        period: 'DAYS',
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchInternalDefects.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.data = action.payload.data
            state.period = action.payload.period
        },
        [fetchInternalDefects.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default internalDefectsSlice.reducer;

export const internalDefectsAggregationsSelector = state => {
    return state.internalDefects.data;
}

export const internalDefectsPeriodSelector = state => state.internalDefects.period;