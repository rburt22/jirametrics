import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {aggregateCreationTotals} from '../../api/totals_api';


export const fetchCreationTotals = createAsyncThunk('aggregates/fetchCreationTotals',
    async (data) => {
        const response = await aggregateCreationTotals(data);
        return {
            data: response.data,
            period: data.period
        }
    })

export const creationTotalsSlice = createSlice({
    name: "creationTotals",
    initialState: {
        data: [],
        period: 'DAYS',
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchCreationTotals.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.data = action.payload.data
            state.period = action.payload.period
        },
        [fetchCreationTotals.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default creationTotalsSlice.reducer;

export const creationTotalsSelector = state => {
    return state.creationTotals.data;
}

export const creationTotalsPeriodSelector = state => state.creationTotals.period;