import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {login} from '../../api/auth_api';


export const authenticateUser = createAsyncThunk('auth/authenticateUser', async (credentials) => {
    await login(credentials);
    return {
        loggedIn: true
    }
})

export const authSlice = createSlice({
    name: "auth",
    initialState: {
        loggedIn: false,
        status: 'idle',
        error: null
    },
    extraReducers: {
        [authenticateUser.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.loggedIn = action.payload.loggedIn;
        },
        [authenticateUser.rejected]: (state, action) => {
            console.log(action);
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default authSlice.reducer;

export const isLoggedInSelector = state => {
    return state.auth.loggedIn;
}

export const authErrorSelector = state => state.auth.error;