import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {getProjectsList} from "../../api/lookups_api";


export const fetchProjectsList = createAsyncThunk('lookups/fetchProjectsList',
    async () => {
        const response = await getProjectsList();
        return {
            projectsList: response.data,
        };
    });

export const lookupSlice = createSlice({
    name: "lookup",
    initialState: {
        projectsList: [],
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchProjectsList.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.projectsList = action.payload.projectsList
        },
        [fetchProjectsList.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default lookupSlice.reducer;

export const projectsListSelector = state => {
    return state.lookup.projectsList;
}
