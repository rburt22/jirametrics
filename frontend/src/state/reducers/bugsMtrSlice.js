import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {aggregateBugsMtr} from '../../api/mtr_api';


export const fetchAggregatesForBugs = createAsyncThunk('aggregates/fetchAggregatesForBugs', async (data) => {
    const response =  await aggregateBugsMtr(data);
    return {
        data: response.data,
        period: data.period
    }
})

export const bugsMtrSlice = createSlice({
    name: "bugsMtr",
    initialState: {
        data: [],
        period: 'DAYS',
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchAggregatesForBugs.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.data = action.payload.data
            state.period = action.payload.period
        },
        [fetchAggregatesForBugs.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default bugsMtrSlice.reducer;

export const bugsAggregationsSelector = state =>  {
    return state.bugsMtr.data;
}

export const bugsPeriodSelector = state => state.bugsMtr.period;