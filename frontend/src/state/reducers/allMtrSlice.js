import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {aggregateMtr} from '../../api/mtr_api';


export const fetchAggregatesForAll = createAsyncThunk('aggregates/fetchAggregates', async (data) => {
    const response =  await aggregateMtr(data);
    return {
        data: response.data,
        period: data.period
    }
})

export const allMtrSlice = createSlice({
    name: "mtr",
    initialState: {
        data: [],
        period: 'DAYS',
        status: 'idle',
        error: null
    },
    extraReducers: {
        [fetchAggregatesForAll.fulfilled]: (state, action) => {
            state.status = 'succeeded'
            state.data = action.payload.data
            state.period = action.payload.period
        },
        [fetchAggregatesForAll.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export default allMtrSlice.reducer;

export const allAggregationsSelector = state =>  {
    return state.mtr.data;
}

export const allPeriodSelector = state => state.mtr.period;