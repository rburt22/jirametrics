import {configureStore} from "@reduxjs/toolkit";
import allMtrReducer from './reducers/allMtrSlice';
import bugsMtrReducer from './reducers/bugsMtrSlice';
import externalDefectsReducer from './reducers/externalDefectsSlice';
import internalDefectsReducer from './reducers/internalDefectsSlice';
import lookupReducer from './reducers/lookupSlice';
import authSlice from "./reducers/authSlice";
import creationTotalsSlice from "./reducers/creationTotalsSlice";
import resolutionTotalsSlice from "./reducers/resolutionTotalsSlice";
import openTotalsSlice from "./reducers/openTotalsSlice";

const store = configureStore({
    reducer: {
        mtr: allMtrReducer,
        bugsMtr: bugsMtrReducer,
        externalDefects: externalDefectsReducer,
        internalDefects: internalDefectsReducer,
        lookup: lookupReducer,
        auth: authSlice,
        creationTotals: creationTotalsSlice,
        resolutionTotals: resolutionTotalsSlice,
        openTotals: openTotalsSlice
    }
})

export default store;