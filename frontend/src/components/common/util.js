import moment from "moment";

export const calculateDate = (dateString, period) => {
    const dateFormat = 'YYYY-MM-DD';
    if (period === 'WEEKS') {
        return moment(dateString, 'YYYY-ww').format(dateFormat);
    }
    if (period === 'MONTHS') {
        return moment(dateString, 'YYYY/MM').format(dateFormat);
    }
    return moment(dateString).format(dateFormat);
}
