import {makeStyles} from "@material-ui/core/styles";


export const usePanelStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 750,
    },
    noFlexBase: {
        flexBasis: 'auto'
    }
}));
