import React from 'react';
import {useSelector} from "react-redux";
import BaseChart from "../common/BaseChart";
import {
    externalDefectsAggregationsSelector,
    externalDefectsPeriodSelector
} from "../../../state/reducers/externalDefectsSlice";
import {createDataForDefects, tooltipPropData} from "./util";


export default function ExternalDefectsChart() {

    const data = useSelector(externalDefectsAggregationsSelector);
    const period = useSelector(externalDefectsPeriodSelector);

    return (
        <BaseChart data={data}
                   period={period}
                   createData={createDataForDefects}
                   yLegend={'External defects'}
                   chartProps={tooltipPropData}
        />
    );

};