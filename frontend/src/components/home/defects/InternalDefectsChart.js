import React from 'react';
import {useSelector} from "react-redux";
import BaseChart from "../common/BaseChart";
import {
    internalDefectsAggregationsSelector,
    internalDefectsPeriodSelector
} from "../../../state/reducers/internalDefectsSlice";
import {createDataForDefects, tooltipPropData} from "./util";


export default function InternalDefectsChart() {

    const data = useSelector(internalDefectsAggregationsSelector);
    const period = useSelector(internalDefectsPeriodSelector);

    return (
        <BaseChart data={data}
                   period={period}
                   createData={createDataForDefects}
                   yLegend={'Internal defects'}
                   chartProps={tooltipPropData}
        />
    );

}