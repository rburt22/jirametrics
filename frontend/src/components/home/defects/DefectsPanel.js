import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import AggregationForm from "../common/AggregationForm";
import {usePanelStyles} from "../../common/styles";
import ExternalDefectsChart from "./ExternalDefectsChart";
import InternalDefectsChart from "./InternalDefectsChart";
import {fetchExternalDefects} from "../../../state/reducers/externalDefectsSlice";
import {fetchInternalDefects} from "../../../state/reducers/internalDefectsSlice";
import clsx from "clsx";

export default function DefectsPanel() {

    const classes = usePanelStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const unit = 'CREATION';

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6} className={classes.noFlexBase}>
                <Paper className={fixedHeightPaper}>
                    <Grid item xs={12} md={12} lg={12} className={classes.noFlexBase}>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Defect escape rate per 100 tasks (External)
                        </Typography>
                        <AggregationForm fetchAction={fetchExternalDefects} unit={unit}/>
                    </Grid>
                    <Grid item lg={12}>
                        <ExternalDefectsChart/>
                    </Grid>
                </Paper>
            </Grid>
            <Grid item xs={12} md={12} lg={6} className={classes.noFlexBase}>
                <Paper className={fixedHeightPaper}>
                    <Grid item xs={12} md={12} lg={12} className={classes.noFlexBase}>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Defect escape rate per 100 tasks (Internal)
                        </Typography>
                        <AggregationForm fetchAction={fetchInternalDefects} unit={unit}/>
                    </Grid>
                    <Grid item lg={12}>
                        <InternalDefectsChart/>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    );
};