import {calculateDate} from "../../common/util";
import React from "react";

export const createDataForDefects = (data, period) => {
    const result = [];
    for (let item of data) {
        result.push(
            {
                y: item.value,
                x: calculateDate(item.date, period),
                totalIssues: item.totalIssues,
                totalBugs: item.totalBugs,
            }
        )
    }
    return [{
        id: 'Defects',
        data: result
    }];
}

export const tooltipPropData = {
    tooltip: ({point}) => {
        const {data} = point;
        return (
            <div
                style={{
                    background: 'white',
                    padding: '9px 12px',
                    border: '1px solid #ccc',
                }}
            >
                <div
                    style={{
                        padding: '3px 0',
                    }}
                >
                    <strong>Bugs / Issues:</strong> {data.totalBugs} / {data.totalIssues}
                </div>
                <div
                    style={{
                        padding: '3px 0',
                    }}
                >
                    <strong>Defect rate:</strong> {data.y}
                </div>
            </div>
        )
    }
}