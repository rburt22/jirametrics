import React from 'react';
import {ResponsiveLine} from "@nivo/line";


export default function BaseChart({data, period, createData, yLegend, chartProps}) {

    const computeTickValues = (period) => {
        switch (period) {
            case 'DAYS':
            case 'WEEKS': {
                return 'every 7 days'
            }
            case 'MONTHS': {
                return 'every 1 month'
            }
        }
    }

    return (
        <ResponsiveLine
            height={500}
            data={createData(data, period)}
            margin={{top: 50, right: 110, bottom: 50, left: 60}}
            xScale={{
                type: 'time',
                format: '%Y-%m-%d',
                useUTC: false,
                precision: 'day',
            }}
            xFormat="time:%Y-%m-%d"
            yScale={{
                type: 'linear',
                stacked: false
            }}
            axisLeft={{
                orient: 'left',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: yLegend,
                legendPosition: 'middle'
            }}
            axisBottom={{
                format: '%m/%d',
                tickValues: computeTickValues(period),
                legend: 'Time',
            }}
            enablePointLabel={true}
            pointBorderWidth={2}
            pointBorderColor={{
                from: 'color',
                modifiers: [['darker', 0.3]],
            }}
            useMesh={true}
            enableSlices={false}
            {...chartProps}
        />
    );
};