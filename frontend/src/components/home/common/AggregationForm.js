import React, {useState} from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import useTheme from "@material-ui/core/styles/useTheme";

import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";

import moment from "moment";
import {useDispatch, useSelector} from "react-redux";
import {projectsListSelector} from "../../../state/reducers/lookupSlice";


const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const prioritiesList = [
    'Highest',
    'High',
    'Medium',
    'Low',
    'Lowest']

function getSelectOptionStyles(name, personName, theme) {
    return {
        fontWeight:
            personName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

export default function AggregationForm({fetchAction, unit, includePriorities}) {
    const dispatch = useDispatch();
    const classes = useStyles();
    const theme = useTheme();

    const [period, setPeriod] = useState('DAYS');
    const [fromDate, setFromDate] = useState(moment().format("YYYY-MM-DD"));
    const [toDate, setToDate] = useState(moment().format("YYYY-MM-DD"));

    const [projects, setProjects] = useState([]);

    const [priorities, setPriorities] = useState([]);

    const projectsList = useSelector(projectsListSelector);


    const onPeriodChange = (e) => {
        setPeriod(e.target.value);
    }

    const onFromDateChange = (date, value) => {
        setFromDate(value);
    };

    const onToDateChange = (date, value) => {
        setToDate(value);
    };

    const onSelectChange = (event) => {
        setProjects(event.target.value);
    }

    const onPrioritiesChange = (event) => {
        setPriorities(event.target.value);
    }

    const fetchAggregations = () => {
        const data = {
            projects,
            unit,
            period,
            from: fromDate,
            to: toDate,
            priorities
        }
        dispatch(fetchAction(data))
    };



    return (
        <form className={classes.root} noValidate autoComplete="off">
            <FormControl className={classes.formControl}>
                <InputLabel id="projects-label">Projects</InputLabel>
                <Select
                    labelId="projects-label"
                    id="projects"
                    multiple
                    required={true}
                    value={projects}
                    onChange={onSelectChange}
                    input={<Input/>}
                    MenuProps={MenuProps}
                >
                    {projectsList.map((project) => (
                        <MenuItem key={project} value={project} style={getSelectOptionStyles(project, projects, theme)}>
                            {project}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
                <InputLabel id="period">Period</InputLabel>
                <Select
                    labelId="period"
                    id="period-select"
                    value={period}
                    onChange={onPeriodChange}
                >
                    <MenuItem value={'DAYS'}>Days</MenuItem>
                    <MenuItem value={'WEEKS'}>Weeks</MenuItem>
                    <MenuItem value={'MONTHS'}>Months</MenuItem>
                </Select>
            </FormControl>
            {includePriorities &&
                <FormControl className={classes.formControl}>
                    <InputLabel id="priorities-label">Priority</InputLabel>
                    <Select
                        labelId="priorities-label"
                        id="priorities"
                        multiple
                        required={true}
                        value={priorities}
                        onChange={onPrioritiesChange}
                        input={<Input/>}
                        MenuProps={MenuProps}
                    >
                        {prioritiesList.map((priority) => (
                            <MenuItem key={priority} value={priority} style={getSelectOptionStyles(priority, prioritiesList, theme)}>
                                {priority}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            }

            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
                <KeyboardDatePicker
                    autoOk
                    disableToolbar
                    variant="inline"
                    format="YYYY-MM-DD"
                    margin="normal"
                    id="from-date"
                    label="From"
                    value={fromDate}
                    onChange={onFromDateChange}
                />
            </MuiPickersUtilsProvider>
            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
                <KeyboardDatePicker
                    autoOk
                    disableToolbar
                    variant="inline"
                    format="YYYY-MM-DD"
                    margin="normal"
                    id="to-date"
                    label="To"
                    value={toDate}
                    onChange={onToDateChange}
                />
            </MuiPickersUtilsProvider>
            <Button onClick={fetchAggregations} variant="contained" color="primary">
                Search
            </Button>
        </form>
    )
}