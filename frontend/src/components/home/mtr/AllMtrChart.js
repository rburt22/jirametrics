import React from 'react';
import {useSelector} from "react-redux";
import {allAggregationsSelector, allPeriodSelector} from "../../../state/reducers/allMtrSlice";
import BaseChart from "../common/BaseChart";
import {createDataForMtr} from "./util";


export default function AllMtrChart() {

    const data = useSelector(allAggregationsSelector);
    const period = useSelector(allPeriodSelector);

    return (
        <BaseChart data={data}
                   period={period}
                   createData={createDataForMtr}
                   yLegend={'MTR'}
        />
    );

}