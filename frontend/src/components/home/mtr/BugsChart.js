import React from 'react';
import {useSelector} from "react-redux";
import {bugsAggregationsSelector, bugsPeriodSelector} from "../../../state/reducers/bugsMtrSlice";
import BaseChart from "../common/BaseChart";
import {createDataForMtr} from "./util";


export default function BugsChart() {

    const data = useSelector(bugsAggregationsSelector);
    const period = useSelector(bugsPeriodSelector);

    return (
        <BaseChart data={data}
                   period={period}
                   createData={createDataForMtr}
                   yLegend={'Bugs MTR'}
        />
    );

}