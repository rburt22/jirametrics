import {calculateDate} from "../../common/util";

export const createDataForMtr = (data, period) => {
    const result = [];
    for (let item of data) {
        result.push(
            {
                y: Math.ceil(item.mtr / 1440),
                x: calculateDate(item.date, period),
            }
        )
    }
    return [{
        id: 'MTR',
        data: result
    }];
}