import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import AggregationForm from "../common/AggregationForm";
import {fetchAggregatesForAll} from "../../../state/reducers/allMtrSlice";
import AllMtrChart from "./AllMtrChart";
import {fetchAggregatesForBugs} from "../../../state/reducers/bugsMtrSlice";
import BugsChart from "./BugsChart";
import clsx from "clsx";
import {usePanelStyles} from "../../common/styles";

export default function MtrPanel() {

    const classes = usePanelStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const unit = 'RESOLUTION';

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6} className={classes.noFlexBase}>
                <Paper className={fixedHeightPaper}>
                    <Grid item xs={12} md={12} lg={12} className={classes.noFlexBase}>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Mean time to resolution
                        </Typography>
                        <AggregationForm fetchAction={fetchAggregatesForAll} unit={unit} includePriorities={true}/>
                    </Grid>
                    <Grid item lg={12}>
                        <AllMtrChart/>
                    </Grid>
                </Paper>
            </Grid>
            <Grid item xs={12} md={12} lg={6} className={classes.noFlexBase}>
                <Paper className={fixedHeightPaper}>
                    <Grid item xs={12} md={12} lg={12} className={classes.noFlexBase}>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            High and Highest priority bugs MTR
                        </Typography>
                        <AggregationForm fetchAction={fetchAggregatesForBugs} unit={unit}/>
                    </Grid>
                    <Grid item lg={12}>
                        <BugsChart/>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    );
};