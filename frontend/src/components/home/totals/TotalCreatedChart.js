import React from 'react';
import {useSelector} from "react-redux";
import {creationTotalsPeriodSelector, creationTotalsSelector} from "../../../state/reducers/creationTotalsSlice";
import BaseChart from "../common/BaseChart";
import {createDataForTotal} from "./util";


export default function TotalCreatedChart() {

    const data = useSelector(creationTotalsSelector);
    const period = useSelector(creationTotalsPeriodSelector);

    return (
        <BaseChart data={data}
                   period={period}
                   createData={createDataForTotal}
                   yLegend={'Total created'}
        />
    );

}