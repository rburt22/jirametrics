import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import AggregationForm from "../common/AggregationForm";
import {fetchCreationTotals} from "../../../state/reducers/creationTotalsSlice";
import TotalCreatedChart from "./TotalCreatedChart";
import {fetchResolutionTotals} from "../../../state/reducers/resolutionTotalsSlice";
import TotalResolvedChart from "./TotalResolvedChart";
import {fetchOpenTotals} from "../../../state/reducers/openTotalsSlice";
import TotalOpenChart from "./TotalOpenChart";
import clsx from "clsx";
import {usePanelStyles} from "../../common/styles";

export default function TotalsPanel() {

    const classes = usePanelStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const unit = 'RESOLUTION';

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6} className={classes.noFlexBase}>
                <Paper className={fixedHeightPaper}>
                    <Grid item xs={12} md={12} lg={12} className={classes.noFlexBase}>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Total issues created
                        </Typography>
                        <AggregationForm fetchAction={fetchCreationTotals} unit={unit} includePriorities={true}/>
                    </Grid>
                    <Grid item lg={12}>
                        <TotalCreatedChart/>
                    </Grid>
                </Paper>
            </Grid>
            <Grid item xs={12} md={12} lg={6} className={classes.noFlexBase}>
                <Paper className={fixedHeightPaper}>
                    <Grid item xs={12} md={12} lg={12} className={classes.noFlexBase}>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Total issues resolved
                        </Typography>
                        <AggregationForm fetchAction={fetchResolutionTotals} unit={unit} includePriorities={true}/>
                    </Grid>
                    <Grid item lg={12}>
                        <TotalResolvedChart/>
                    </Grid>
                </Paper>
            </Grid>
            <Grid item xs={12} md={12} lg={6} className={classes.noFlexBase}>
                <Paper className={fixedHeightPaper}>
                    <Grid item xs={12} md={12} lg={12} className={classes.noFlexBase}>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Total open issues
                        </Typography>
                        <AggregationForm fetchAction={fetchOpenTotals} unit={unit} includePriorities={true}/>
                    </Grid>
                    <Grid item lg={12}>
                        <TotalOpenChart />
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    );
};