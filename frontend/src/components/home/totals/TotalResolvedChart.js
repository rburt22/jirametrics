import React from 'react';
import {useSelector} from "react-redux";
import {resolutionTotalsPeriodSelector, resolutionTotalsSelector} from "../../../state/reducers/resolutionTotalsSlice";
import BaseChart from "../common/BaseChart";
import {createDataForTotal} from "./util";


export default function TotalResolvedChart() {

    const data = useSelector(resolutionTotalsSelector);
    const period = useSelector(resolutionTotalsPeriodSelector);

    return (
        <BaseChart data={data}
                   period={period}
                   createData={createDataForTotal}
                   yLegend={'Total resolved'}
        />
    );

}