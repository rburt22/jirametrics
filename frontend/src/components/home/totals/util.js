import {calculateDate} from "../../common/util";
import React from "react";

export const createDataForTotal = (data, period) => {
    const result = [];
    for (let item of data) {
        result.push(
            {
                y: item.value,
                x: calculateDate(item.date, period),
            }
        )
    }
    return [{
        id: 'Totals',
        data: result
    }];
}
