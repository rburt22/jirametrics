import React from 'react';
import {useSelector} from "react-redux";
import {openTotalsSelector, openTotalsPeriodSelector} from "../../../state/reducers/openTotalsSlice";
import BaseChart from "../common/BaseChart";
import {createDataForTotal} from "./util";


export default function TotalOpenChart() {

    const data = useSelector(openTotalsSelector);
    const period = useSelector(openTotalsPeriodSelector);

    return (
        <BaseChart data={data}
                   period={period}
                   createData={createDataForTotal}
                   yLegend={'Total open'}
        />
    );

}